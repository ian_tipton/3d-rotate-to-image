//
//  ViewController.h
//  3DTransformToImage
//
//  Created by Ian Tipton on 09/05/2013.
//  Copyright (c) 2013 Ian Tipton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *transformedImageView;

- (IBAction)takeScreenshot:(id)sender;
@end
