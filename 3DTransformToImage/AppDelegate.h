//
//  AppDelegate.h
//  3DTransformToImage
//
//  Created by Ian Tipton on 09/05/2013.
//  Copyright (c) 2013 Ian Tipton. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
