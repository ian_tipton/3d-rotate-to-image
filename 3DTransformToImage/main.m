//
//  main.m
//  3DTransformToImage
//
//  Created by Ian Tipton on 09/05/2013.
//  Copyright (c) 2013 Ian Tipton. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
